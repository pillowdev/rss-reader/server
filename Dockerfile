FROM maven:3.5.2-jdk-8-alpine AS build

RUN apk --no-cache add ca-certificates wget && wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://raw.githubusercontent.com/sgerrand/alpine-pkg-glibc/master/sgerrand.rsa.pub && wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.27-r0/glibc-2.27-r0.apk && apk add glibc-2.27-r0.apk && apk add --no-cache protobuf

ADD app /usr/src/rss

RUN mvn clean install -f /usr/src/rss -e

FROM vertx/vertx3 AS run

WORKDIR /usr/verticles

COPY --from=build /usr/src/rss/target/rssr-0.1.jar /usr/verticles/verticle.jar

ENV VERTICLE_NAME io.vertx.rss.reader.MainVerticle

ENV VERTICLE_HOME /usr/verticles

ENTRYPOINT ["sh", "-c"]

ENV HTTP_ADDR 0.0.0.0

ENV HTTP_PORT 8080

EXPOSE 8080

ADD sc.json /tmp

CMD ["exec vertx run $VERTICLE_NAME -cp $VERTICLE_HOME/*"]

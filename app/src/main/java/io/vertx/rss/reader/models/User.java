package io.vertx.rss.reader.models;

import java.util.ArrayList;
import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;

/*
 *
 * {
 *    "firebase_id": string,
 *    "reads": [
 *          {
 *          "feed": string, //url of the feed
 *          "id": int
 *        }
 *     ]
 *    "feeds": [
 *      string, //url of the feed
 *    ]
 *
 */

public class User {

  public String firebaseID;
  public JsonArray feeds;
  public JsonArray reads;
  public JsonArray favorites;

  public User(String id) {
    firebaseID = id;
    feeds = new JsonArray();
    reads = new JsonArray();
    favorites = new JsonArray();
  }

  public User() {
    firebaseID = "";
    feeds = new JsonArray();
    reads = new JsonArray();
    favorites = new JsonArray();
  }

  public JsonObject toJson() {
    JsonObject ret = new JsonObject();
    ret.put("firebase_id", firebaseID);
    ret.put("reads", reads);
    ret.put("feeds", feeds);
    ret.put("favorites", favorites);
    return ret;
  }
}

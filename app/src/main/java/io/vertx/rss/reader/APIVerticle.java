package io.vertx.rss.reader;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.handler.*;
import io.vertx.ext.web.Router;
import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;
import io.vertx.core.http.HttpMethod;
import java.util.Set;
import java.util.HashSet;

import io.vertx.rss.reader.api.ApiClass;

public class APIVerticle extends AbstractVerticle{

  private HttpServer server;
  private ApiClass api;

  public void init(Vertx vertx, Context context) {
    super.init(vertx, context);
  }

  public void start(Future<Void> future) {
    server = vertx.createHttpServer();
    api = new ApiClass(vertx);
    server.requestHandler(buildHTTPRouter()::accept);
    server.listen(Integer.parseInt(System.getenv("HTTP_PORT")), System.getenv("HTTP_ADDR"), result -> {
      if (result.succeeded()) {
        future.complete();
      } else {
        future.fail(result.cause());
      }
    });
  }

  public void stop(Future<Void> future) {
    future.complete();
  }

  private Router applyCors(Router router) {
    //METHODS
    Set<HttpMethod> allowedMethods = new HashSet<>();
    allowedMethods.add(HttpMethod.GET);
    allowedMethods.add(HttpMethod.POST);
    allowedMethods.add(HttpMethod.OPTIONS);
    allowedMethods.add(HttpMethod.DELETE);
    allowedMethods.add(HttpMethod.PATCH);
    allowedMethods.add(HttpMethod.PUT);

    //HEADERS
    Set<String> allowedHeaders = new HashSet<>();
    allowedHeaders.add("x-requested-with");
    allowedHeaders.add("Access-Control-Allow-Origin");
    allowedHeaders.add("origin");
    allowedHeaders.add("Content-Type");
    allowedHeaders.add("accept");
    allowedHeaders.add("X-PINGARUNER");
    allowedHeaders.add("Authorization");

    router.route().handler(CorsHandler.create(".*").allowedHeaders(allowedHeaders).allowedMethods(allowedMethods));
    return router;
  }

  private Router buildHTTPRouter() {
    Router router = Router.router(vertx);
    router.route().failureHandler(ErrorHandler.create(true));

    router.route().consumes("application/json");
    router.route().produces("application/json");
    router.route().handler(BodyHandler.create());
    router.route().handler(context -> {
      context.response().headers().add(CONTENT_TYPE, "application/json");
      context.next();
    });


    router.route().handler(api::firebaseHandler);
    router = applyCors(router);
    router.post("/v1/articles").handler(api::accessArticles);
    router.get("/v1/articles").handler(api::getArticles);

    router.post("/v1/favorite/:uid/:offset").handler(api::addFavorite);
    router.get("/v1/favorites").handler(api::getFavorites);
    router.delete("/v1/favorites/:uid/:offset").handler(api::deleteFavorite);

    router.post("/v1/feeds").handler(api::addFeed);
    router.get("/v1/feeds").handler(api::getFeeds);
    router.get("/v1/feeds/:uid").handler(api::getFeedInfo);
    router.delete("/v1/feeds/:uid").handler(api::removeFeed);

    router.post("/v1/reads/:uid/:offset").handler(api::acknowledgeArticle);
    router.get("/v1/reads").handler(api::getReads);

    return router;
  }
}

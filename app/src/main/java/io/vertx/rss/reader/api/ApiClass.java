package io.vertx.rss.reader.api;

import org.apache.commons.codec.binary.Hex;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.RoutingContext;
import io.vertx.rss.reader.dao.MongoDAO;
import io.vertx.rss.reader.exceptions.*;
import io.vertx.rss.reader.models.Feed;
import io.vertx.rss.reader.utils.*;
import io.vertx.rss.reader.utils.Feeder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import io.vertx.core.http.HttpMethod;

public class ApiClass {

  public static Integer HTTP_INTERNAL_ERROR = 500;
  public static Integer HTTP_OK = 200;
  public static Integer HTTP_OK_EMPTY = 202;
  public static Integer HTTP_BAD_REQUEST = 400;
  public static Integer HTTP_AUTH = 401;
  public static Integer HTTP_AUTHOR = 403;
  public static Integer HTTP_NOT_FOUND = 404;
  public static JsonObject HTTP_INTERNAL_ERROR_MESSAGE = new JsonObject().put("success", false).put("message", "internal server error");
  public static JsonObject HTTP_AUTH_MESSAGE = new JsonObject().put("success", false).put("message", "Can't authenticate");
  public static JsonObject HTTP_AUTHOR_MESSAGE = new JsonObject().put("success", false).put("message", "Access denied");
  public static JsonObject HTTP_NOT_FOUND_MESSAGE = new JsonObject().put("success", false).put("message", "not found");
  public static JsonObject HTTP_BAD_REQUEST_MESSAGE = new JsonObject().put("success", false).put("message", "bad request");
  public static JsonObject HTTP_OK_MESSAGE = new JsonObject().put("success", true).put("message", "ok");
  public static String DEFAULT_TOKEN = "mabite";

  private MongoDAO dao;
  private Vertx vhandler;

  public ApiClass(Vertx vertx) {
    JsonObject cfg = buildConfig();
    MongoClient mc = MongoClient.createShared(vertx, cfg);
    vhandler = vertx;
    dao = new MongoDAO(mc);
  }


  private JsonObject buildConfig() {
    JsonObject cfg = new JsonObject();
    //TODO: FROM ENV
    cfg.put("connection_string", "mongodb://172.17.0.1:27017");
    cfg.put("db_name", "rss");
    return cfg;
  }

  public void getArticles(RoutingContext context) {
    dao.findAllDistinctArticles(res -> {
      if (res.failed()) {
        reply(HTTP_INTERNAL_ERROR, HTTP_INTERNAL_ERROR_MESSAGE, context);
      } else {
        reply(new JsonObject().put("error", new JsonArray()).put("articles", new JsonArray(res.result())), context);
      }
    });
  }

  public void accessArticles(RoutingContext context) {
    JsonObject obj = context.getBodyAsJson();

    JsonObject body = obj.getJsonArray("feeds").getJsonObject(0);
    String feedId = body.getString("uid");
    Integer offset = Integer.parseInt(body.getString("offset"));
    Integer ammount = Integer.parseInt(body.getString("ammount"));
    JsonObject user = context.get("user");
    JsonArray userFeeds = user.getJsonArray("feeds");
    for (int i = 0; i < userFeeds.size(); i++) {
      if (userFeeds.getString(i).equals(feedId)) {
        dao.retrieveArticle(feedId, offset, ammount, retrieve -> {
          if (retrieve.failed()) {
            reply(HTTP_INTERNAL_ERROR, HTTP_INTERNAL_ERROR_MESSAGE, context);
          } else if (retrieve.result() == null) {
            reply(HTTP_NOT_FOUND, HTTP_NOT_FOUND_MESSAGE, context);
          } else {
            JsonObject response = new JsonObject().put("errors", new JsonArray());
            JsonObject articles = new JsonObject()
            .put("uid", feedId)
            .put("articles", new JsonArray(retrieve.result()));
            response.put("articles", articles);
            reply(response, context);
          }
        });
      }
    }
  }

  public void deleteFavorite(RoutingContext context) {
    JsonObject user = context.get("user");
    String feedId = context.request().getParam("uid");
    String offset = context.request().getParam("offset");
    JsonObject q = new JsonObject();
    q.put("offset", offset).put("sourceUid", feedId);
    dao.remElemArrayUser(user.getString("firebase_id"), "favorites", q, res -> {
      if (res.failed()) {
        reply(HTTP_INTERNAL_ERROR, HTTP_INTERNAL_ERROR_MESSAGE, context);
      } else if (res.result() == null) {
        reply(HTTP_NOT_FOUND, HTTP_NOT_FOUND_MESSAGE, context);
      } else {
        reply(context);
      }
    });
  }

  public void addFavorite(RoutingContext context) {
    JsonObject user = context.get("user");
    String feedId = context.request().getParam("uid");
    String offset = context.request().getParam("offset");
    JsonArray favs = user.getJsonArray("favorites");
    for (int i = 0; i < favs.size(); i++) {
      JsonObject item = favs.getJsonObject(i);
      if (item.getString("sourceUid").equals(feedId) && item.getString("offset").equals(offset)) {
        reply(context);
        return;
      }
    }

    dao.findArticleByFeedAndOffset(feedId, Integer.parseInt(offset), find -> {
      if (find.failed()) {
        reply(HTTP_INTERNAL_ERROR, HTTP_INTERNAL_ERROR_MESSAGE, context);
      } else if (find.result() == null) {
        reply(HTTP_NOT_FOUND, HTTP_NOT_FOUND_MESSAGE, context);
      } else {
        JsonObject q = new JsonObject().put("offset", String.valueOf(offset)).put("sourceUid", feedId);
        dao.updateArrayInUser(user.getString("firebase_id"), "favorites", q, res -> {
          if (res.failed()) {
            reply(HTTP_INTERNAL_ERROR, HTTP_INTERNAL_ERROR_MESSAGE, context);
          } else if (res.result() == null) {
            reply(HTTP_NOT_FOUND, HTTP_NOT_FOUND_MESSAGE, context);
          } else {
            reply(context);
          }
        });
      }
    });
  }

  public void getFavorites(RoutingContext context) {
    JsonObject user = context.get("user");
    JsonObject resp = new JsonObject()
    .put("location", user.getJsonArray("favorites"));
    reply(resp, context);
  }

  public void getFeedInfo(RoutingContext context) {
    String id = context.request().getParam("uid");
    dao.findFeedById(id, res -> {
      if (res.failed()) {
        reply(HTTP_NOT_FOUND, HTTP_NOT_FOUND_MESSAGE, context);
        return;
      }
      reply(HTTP_OK, res.result(), context);
    });
  }

  public void addFeed(RoutingContext context) {
    JsonObject body = context.getBodyAsJson();
    Feed f = new Feed();
    f.source = body.getString("source");

    try {
      MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
      messageDigest.update(f.toJson().toString().getBytes());
      f.uid = new String(Hex.encodeHex(messageDigest.digest()));
    } catch (NoSuchAlgorithmException exc){
      reply(HTTP_INTERNAL_ERROR, HTTP_INTERNAL_ERROR_MESSAGE, context);
      return;
    }

    //test if feed exists
    dao.findFeedById(f.uid, find -> {
      System.out.println(find.result());
      if (find.failed()) {
        reply(HTTP_INTERNAL_ERROR, HTTP_INTERNAL_ERROR_MESSAGE, context);
        return;
      } else if (find.result() == null) {
          System.out.printf("The feed %s does not exists\n", f.uid);
          //Feeds does not exists
          vhandler.setTimer(1000, id -> {
            Feeder.doCreate(vhandler, f.toJson(), dao);
          });
      }
    });
    JsonObject user = context.get("user");

    dao.updateArrayInUser(user.getString("firebase_id"), "feeds", f.uid, cr -> {
        if (cr.succeeded()) {
          System.out.println(cr.result());
          reply(context);
          return;
        } else {
          reply(HTTP_INTERNAL_ERROR, HTTP_INTERNAL_ERROR_MESSAGE, context);
          return;
        }
    });
  }

  public void getFeeds(RoutingContext context) {
    JsonObject user = context.get("user");
    JsonArray userFeeds = user.getJsonArray("feeds");
    if (userFeeds == null) {
      reply(HTTP_NOT_FOUND, HTTP_NOT_FOUND_MESSAGE, context);
      return;
    }
    dao.getFeedsInfo(userFeeds, infos -> {
      if (infos.succeeded()) {
        System.out.println(infos.result());
        reply(new JsonObject().put("feeds", new JsonArray(infos.result())), context);
      }
    });
  }

  public void removeFeed(RoutingContext context) {
    String uid = context.request().getParam("uid");
    JsonObject user = context.get("user");
    dao.remElemArrayUser(user.getString("firebase_id"), "feeds", uid, res -> {
      if (res.failed()) {
        reply(HTTP_INTERNAL_ERROR, HTTP_INTERNAL_ERROR_MESSAGE, context);
      } else if (res.result() == null) {
        reply(HTTP_NOT_FOUND, HTTP_NOT_FOUND_MESSAGE, context);
      } else {
        reply(context);
      }
    });
  }

  public void getReads(RoutingContext context) {
    JsonObject user = context.get("user");
    JsonObject resp = new JsonObject()
    .put("location", user.getJsonArray("reads"));
    reply(resp, context);
  }

  public void acknowledgeArticle(RoutingContext context) {
    JsonObject user = context.get("user");
    String feedId = context.request().getParam("uid");
    String offset = context.request().getParam("offset");

    JsonObject q = new JsonObject().put("offset", offset).put("sourceUid", feedId);
    dao.updateArrayInUser(user.getString("firebase_id"), "reads", q, res -> {
      if (res.failed()) {
        reply(HTTP_INTERNAL_ERROR, HTTP_INTERNAL_ERROR_MESSAGE, context);
      } else if (res.result() == null) {
        reply(HTTP_NOT_FOUND, HTTP_NOT_FOUND_MESSAGE, context);
      } else {
        reply(context);
      }
    });
  }

  public void defaultHandler(RoutingContext context) {
    JsonObject body = context.getBodyAsJson();
    System.out.println(body.toString());
    context.response().end("{\"mabite\":\"existe\"}");
  }

  public void firebaseHandler(RoutingContext context) {
    if (context.request().method() == HttpMethod.OPTIONS) {
      context.next();
      return;
    }
    try {
      String token = Firebase.retrieveTokenFromContext(context);
      if (System.getenv("AUTH_DEBUG") == null || token.equals(new String(DEFAULT_TOKEN))) {
        token = Firebase.verifyToken(token);
      }
      dao.findOrCreateUser(token, res -> {
        if (res.succeeded()) {
          injectUser(context, res.result());
          context.next();
          return;
        } else {
          context.fail(500);
          return;
        }
      });
    } catch (AuthenticationException exc) {
      reply(HTTP_AUTHOR, HTTP_AUTHOR_MESSAGE, context);
      return;
    } catch (APIException exc) {
      reply(HTTP_BAD_REQUEST, HTTP_BAD_REQUEST_MESSAGE, context);
      return;
    }
  }

  private void reply(Integer code, JsonObject response, RoutingContext context) {
    context.response().setStatusCode(code).end(response.toString());
  }

  private void reply(RoutingContext context) {
    context.response().setStatusCode(HTTP_OK).end(HTTP_OK_MESSAGE.toString());
  }

  private void reply(JsonObject response, RoutingContext context) {
    context.response().setStatusCode(HTTP_OK).end(response.toString());
  }

  private void injectUser(RoutingContext context, JsonObject user) {
    context.put("user", user);
  }
}

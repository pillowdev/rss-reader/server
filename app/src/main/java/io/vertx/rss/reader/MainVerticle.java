package io.vertx.rss.reader;

import io.vertx.core.*;
import java.lang.System;
import io.vertx.rss.reader.utils.*;

public class MainVerticle extends AbstractVerticle {

    private String deploymentId;

    @Override
  	public void init(Vertx vertx, Context context) {
        super.init(vertx, context);
    }

    @Override
    public void start(Future<Void> future) {
      Firebase.initFB();
      vertx.deployVerticle("io.vertx.rss.reader.APIVerticle", res -> {
          System.out.println(res.succeeded());
          if (res.succeeded()) {
              deploymentId = res.result();
          } else {
              res.cause().printStackTrace();
              System.out.printf("deployment failed due to %s\n", res.cause());
          }
      });
      future.complete();
    }

    @Override
    public void stop(Future<Void> future) {
        vertx.undeploy(deploymentId, res -> {
            if (res.succeeded()) {
                System.out.println("undeployment suceeded, exiting gracefully");
            } else {
                System.out.println("undeployment failed, some verticles may remains");
                future.fail("undeployment failed");
            }
        });
        future.complete();
    }
}

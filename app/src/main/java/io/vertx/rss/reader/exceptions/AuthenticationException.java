package io.vertx.rss.reader.exceptions;

public class AuthenticationException extends Exception {
  public AuthenticationException(String message) {
    super(message);
  }
}

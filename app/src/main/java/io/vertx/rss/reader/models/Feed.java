package io.vertx.rss.reader.models;

import io.vertx.core.json.JsonObject;

public class Feed {

  public String uid;
  public String source;
  public String title;
  public String link;
  public String description;
  public Integer currentOffset;
  public String language;
  public String copyright;
  public String lastBuildDate;
  public String imageUrl;
  public String ttl;
  public String recvUnixDate;

  public void fromJson(JsonObject json) {
    uid = json.getJsonObject("uid").getString("uid");
    source = json.getJsonObject("source").getString("source");
    title = json.getString("title");
    link = json.getString("link");
    description = json.getString("description");
    currentOffset = json.getInteger("currentOffset");
    language = json.getString("language");
    copyright = json.getString("copyright");
    lastBuildDate = json.getString("lastBuildDate");
    imageUrl = json.getString("imageUrl");
    ttl = json.getString("ttl");
    recvUnixDate = json.getString("recvUnixDate");
  }

  public JsonObject toJson() {
    return new JsonObject()
    .put("uid", new JsonObject().put("uid", uid))
    .put("source", new JsonObject().put("source", source))
    .put("title", title)
    .put("link", link)
    .put("description", description)
    .put("currentOffset", currentOffset)
    .put("language", language)
    .put("copyright", copyright)
    .put("lastBuildDate", lastBuildDate)
    .put("imageUrl", imageUrl)
    .put("ttl", ttl)
    .put("recvUnixDate", recvUnixDate);
  }

}

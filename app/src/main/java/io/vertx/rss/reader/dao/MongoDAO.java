package io.vertx.rss.reader.dao;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;
import io.vertx.rss.reader.models.User;
import io.vertx.rss.reader.models.Feed;
import java.util.List;
import java.lang.Integer;

public class MongoDAO {

  private MongoClient mongo;

  public MongoDAO(MongoClient c) {
    mongo = c;
  }

  public void insertArticles(JsonObject article, Handler<AsyncResult<String>> handler) {
    mongo.insert("articles", article, handler);
  }

  public void findAllDistinctArticles(Handler<AsyncResult<List<JsonObject>>> handler) {
    mongo.find("articles", new JsonObject(), handler);
  }

  public void retrieveArticle(String feedID, Integer offset, Integer ammount, Handler<AsyncResult<List<JsonObject>>> handler) {
    JsonArray queryArray = new JsonArray();
    for (int i = offset + 1; i <= offset + ammount; i++) {
      queryArray.add(i);
    }

    JsonObject query = new JsonObject()
    .put("idx.offset", new JsonObject()
    .put("$in", queryArray));

    mongo.find("articles", query, handler);
  }

  public void updateFeed(String id, JsonObject feed, Handler<AsyncResult<JsonObject>> handler) {
    mongo.findOneAndUpdate("feeds",
    new JsonObject().put("uid", id),
    feed,
    handler);
  }

  public void updateArrayInUser(String id, String array, JsonObject update, Handler<AsyncResult<JsonObject>> handler) {
    mongo.findOneAndUpdate("users",
    new JsonObject().put("firebase_id", id),
    new JsonObject().put("$push",
    new JsonObject().put(array, update)),
    handler);
  }

  public void getFeedsInfo(JsonArray feeds, Handler<AsyncResult<List<JsonObject>>> handler) {
    System.out.println(new JsonObject().put("uid.uid", new JsonObject().put("$in", feeds)).toString());
    mongo.find("feeds",
    new JsonObject().put("uid.uid", new JsonObject().put("$in", feeds)),
    handler);
  }

  public void updateArrayInUser(String id, String array, String update, Handler<AsyncResult<JsonObject>> handler) {
    mongo.findOneAndUpdate("users",
    new JsonObject().put("firebase_id", id),
    new JsonObject().put("$push",
    new JsonObject().put(array, update)),
    handler);
  }

  public void remElemArrayUser(String id, String array, String remove, Handler<AsyncResult<JsonObject>> handler) {
    mongo.findOneAndUpdate("users",
    new JsonObject().put("firebase_id", id),
    new JsonObject().put("$pull",
    new JsonObject().put(array, remove)),
    handler);
  }

  public void remElemArrayUser(String id, String array, JsonObject remove, Handler<AsyncResult<JsonObject>> handler) {
    mongo.findOneAndUpdate("users",
    new JsonObject().put("firebase_id", id),
    new JsonObject().put("$pull",
    new JsonObject().put(array, remove)),
    handler);
  }

  public void updateUser(JsonObject user, Handler<AsyncResult<JsonObject>> handler) {
    mongo.findOneAndUpdate("users",
    new JsonObject().put("firebase_id", user.getString("firebase_id")),
    user, handler);
  }

  public void findOrCreateUser(String fid, Handler<AsyncResult<JsonObject>> handler) {
    JsonObject query = new JsonObject().put("firebase_id", fid);
    mongo.findOne("users", query, null, find -> {
      if (find.succeeded() && find.result() == null) {
        query.put("favorites", new JsonArray())
        .put("feeds", new JsonArray())
        .put("reads", new JsonArray());
        mongo.insert("users", query, insert -> {
          if (insert.succeeded() && insert.result() != null) {
            mongo.findOne("users", query, null, handler);
          }
        });
      } else {
        mongo.findOne("users", query, null, handler);
      }
    });
  }

  public void findUserById(String fid, Handler<AsyncResult<JsonObject>> handler) {
    JsonObject query = new JsonObject();
    query.put("firebase_id", fid);
    mongo.findOne("user", query, null, handler);
  }

  public void createUser(String fid, Handler<AsyncResult<String>> handler) {
    User u = new User(fid);
    mongo.insert("users", u.toJson(), handler);
  }

  public void createFeed(Feed f, Handler<AsyncResult<String>> handler) {
    mongo.insert("feeds", f.toJson(), handler);
  }

  public void createFeed(JsonObject f, Handler<AsyncResult<String>> handler) {
    mongo.insert("feeds", f, handler);
  }

  public void findArticleByGUID(String guid, Handler<AsyncResult<JsonObject>> handler) {
    mongo.findOne("articles", new JsonObject().put("guid", guid), null, handler);
  }

  public void findAllFeeds(Handler<AsyncResult<List<JsonObject>>> handler) {
    mongo.find("collection", new Feed().toJson(), handler);
  }

  public void findFeedById(String id, Handler<AsyncResult<JsonObject>> handler) {
    JsonObject obj = new JsonObject().put("uid", new JsonObject().put("uid", id));
    mongo.findOne("feeds", obj, null, handler);
  }

  public void findArticleByFeedAndOffset(String id, Integer offset, Handler<AsyncResult<JsonObject>> handler) {
    JsonObject query = new JsonObject()
    .put("idx", new JsonObject()
    .put("sourceUid", id)
    .put("offset", offset));
    mongo.findOne("articles", query, null, handler);
  }

  public void getArticlesFromFeed(String id, Handler<AsyncResult<JsonArray>> handler) {
    JsonObject query = new JsonObject()
    .put("idx", new JsonObject()
    .put("sourceUid", id));
    mongo.distinctWithQuery("articles", "idx.sourceUid", String.class.getName(), query, handler);
  }

  public void deleteFeedById(String id, Handler<AsyncResult<JsonObject>> handler) {
    JsonObject obj = new JsonObject().put("uid", new JsonObject().put("uid", id));
    mongo.findOneAndDelete("feeds", obj, handler);
    System.out.println(obj.toString());
  }
}

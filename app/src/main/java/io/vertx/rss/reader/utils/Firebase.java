package io.vertx.rss.reader.utils;

import com.google.firebase.auth.*;
import io.vertx.ext.web.RoutingContext;
import io.vertx.rss.reader.exceptions.*;
import java.io.FileInputStream;
import java.lang.InterruptedException;
import java.util.concurrent.ExecutionException;
import java.io.FileInputStream;
import com.google.firebase.*;
import com.google.firebase.auth.*;
import com.google.auth.oauth2.*;
import java.io.IOException;

public class Firebase {

  public static String retrieveTokenFromContext(RoutingContext context) throws AuthenticationException {
    String token = context.request().getHeader("Authorization");
    if (token == null) {
      throw new AuthenticationException("Missing authorization header");
    }
    if (token.startsWith("Bearer ")) {
      return token.substring(7);
    } else {
      System.out.println("bad authorr");
      throw new AuthenticationException("Bad authorization format");
    }
  }

  public static String verifyToken(String jwt) throws APIException {
    try {
      FirebaseToken token = FirebaseAuth.getInstance().verifyIdTokenAsync(jwt).get();
      return token.getUid();
    } catch (ExecutionException|InterruptedException exc) {
      throw new APIException(exc.getMessage());
    }
  }

  public static void initFB() {
    try {
      FileInputStream serviceAccount = new FileInputStream("/tmp/sc.json");
      FirebaseOptions options = new FirebaseOptions.Builder()
      .setCredentials(GoogleCredentials.fromStream(serviceAccount))
      .setDatabaseUrl("https://<DATABASE_NAME>.firebaseio.com/")
      .build();
      FirebaseApp.initializeApp(options);
    } catch (IOException exc) {
      System.out.println("Cant start firebase");
      System.exit(2);
    }
  }
}

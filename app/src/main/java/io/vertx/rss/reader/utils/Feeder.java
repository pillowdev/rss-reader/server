package io.vertx.rss.reader.utils;

import io.vertx.core.*;
import io.vertx.core.json.*;
import io.vertx.ext.web.client.*;
import io.vertx.core.buffer.*;
import com.rometools.rome.feed.synd.*;
import com.rometools.rome.io.*;
import java.io.StringReader;
import java.util.Date;
import java.util.List;
import io.vertx.rss.reader.dao.MongoDAO;
import java.net.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.codec.binary.Hex;

public class Feeder {

  private static void getAndStoreFeedFromUrl(Integer offset, String feedurl, JsonObject feed, WebClient c, MongoDAO dao) {
    try {
      URL url = new URL(feedurl);
      System.out.println(url.getPort());
      c.get((url.getPort() < 0) ? 80 : url.getPort(), url.getHost(), url.getPath()).send(ar -> {
        if (ar.failed()) {
          System.out.println("Can't update feed due to");
          System.out.println(ar.cause().getMessage());
        } else {
          HttpResponse<Buffer> response = ar.result();
          if (response.statusCode() != 200) {
            return;
          } else {
            StringReader data = new StringReader(response.bodyAsString());
            SyndFeedInput input = new SyndFeedInput();
            try {
              SyndFeed xmlfeed = input.build(data);
              updateDB(dao, createMeta(feed, xmlfeed), createArticles(xmlfeed, feed, offset), offset);
            } catch (FeedException exc) {
              return;
            }
          }
        }
      });
    } catch (MalformedURLException exc ) {
      System.out.println("wrong url format");
    }
  }

  public static void doUpdate(Vertx vertx, JsonObject feed, MongoDAO dao) {
    dao.getArticlesFromFeed(feed.getJsonObject("source").getString("source"), get -> {
      if (get.failed()) {
        System.out.println("error while getting current article");
      } else if (get.result() == null) {
        System.out.println("no such article for requested feed");
      } else {
        getAndStoreFeedFromUrl(
          get.result().size(),
          feed.getJsonObject("source").getString("source"),
          feed,
          WebClient.create(vertx),
          dao
        );
      }
    });
  }

  public static void doCreate(Vertx vertx, JsonObject feed, MongoDAO dao) {
    System.out.println("I've been called for feed");
    System.out.println(feed.getJsonObject("source").getString("source"));

    getAndStoreFeedFromUrl(
      0,
      feed.getJsonObject("source").getString("source"),
      feed,
      WebClient.create(vertx),
      dao
    );
  }

  public static void updateDB(MongoDAO dao, JsonObject feed, JsonArray articles, Integer offset) {
    //insert articles
    for (int i = 0; i < articles.size(); i++) {

      JsonObject atc = articles.getJsonObject(i);

      dao.findArticleByGUID(atc.getString("guid"), find -> {
        if (find.result() == null) {
          dao.insertArticles(atc, res -> {
            if (res.succeeded() && res.result() != null) {
              System.out.println("insertion article ok");
            }
          });
        } else {
          System.out.println("duplicate in db");
        }
      });
    }
    if (offset != 0) {
      return;
    }
    String feedID = feed.getJsonObject("source").getString("source");
    dao.findFeedById(feedID, find -> {
      if (find.failed()) {
        System.out.println("Feed insertion failed");
      } else if (find.result() == null){
        dao.createFeed(feed, create -> {
          if (create.succeeded() && create.result() != null) {
            System.out.println("insertion feed ok");
          } else {
            System.out.println("Fail why creating the field");
          }
        });
      } else {
        dao.updateFeed(feedID, feed, update -> {
          if (update.succeeded() && update.result() != null) {
            System.out.println("insertion feed ok");
          } else {
            System.out.println("Fail while updating the field");
          }
        });
      }
    });
  }

  public static JsonObject createMeta(JsonObject rssFeed, SyndFeed feed) {
    long now = new Date().getTime() / 1000;
    rssFeed.put("source", new JsonObject().put("source", rssFeed.getJsonObject("source").getString("source")));
    rssFeed.put("title", feed.getTitle());
    rssFeed.put("link", feed.getLink());
    rssFeed.put("description", feed.getDescription());
    rssFeed.put("currentOffset", "0");
    rssFeed.put("language", feed.getLanguage());
    rssFeed.put("copyright", feed.getCopyright());
    rssFeed.put("lastBuildDate", "");
    rssFeed.put("imageUrl", (feed.getImage() == null) ? "" : feed.getImage().getUrl());
    rssFeed.put("recvUnixDate", String.valueOf(now));
    return rssFeed;
  }

  public static JsonArray createArticles(SyndFeed feed, JsonObject dbfeed, Integer offset) {
    JsonArray arr = new JsonArray();
    String uid = dbfeed.getJsonObject("uid").getString("uid");
    List<SyndEntry> articles = feed.getEntries();


    for (int i = 0; i < articles.size(); i++) {
      long now = new Date().getTime() / 1000;
      JsonObject ret = new JsonObject();

      ret.put("title", articles.get(i).getTitle());
      ret.put("link", articles.get(i).getLink());
      ret.put("description", feed.getDescription());
      ret.put("author", articles.get(i).getAuthor());
      ret.put("categories", new JsonArray());
      ret.put("comments", articles.get(i).getComments());
      ret.put("enclosure", new JsonObject().put("url", "").put("length", "").put("mimeType", ""));
      ret.put("pubDate", String.valueOf(articles.get(i).getPublishedDate().getTime() / 1000));
      ret.put("source", new JsonObject().put("url", "").put("description", ""));
      try {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(ret.toString().getBytes());
        ret.put("guid", new String(Hex.encodeHex(messageDigest.digest())));
        ret.put("recvUnixDate", String.valueOf(now));
        ret.put("idx", new JsonObject().put("sourceUid", uid).put("offset", String.valueOf(offset + i)));
        arr.add(ret);
      } catch (NoSuchAlgorithmException exc){
        continue;
      }
    }
    return arr;
  }

}

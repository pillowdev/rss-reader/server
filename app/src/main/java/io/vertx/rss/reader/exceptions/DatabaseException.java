package io.vertx.rss.reader.exceptions;

public class DatabaseException extends Exception {
  public DatabaseException(String message) {
    super(message);
  }
}

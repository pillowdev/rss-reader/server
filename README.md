# Server

## Build

```sh
docker build -t server .
```

## Run Locally

```sh
docker run -itp 8080:8080 server
```

## Access

Start a browser and go on `http://localhost:8080`
